﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PSUTutorcheerup.Migrations
{
    public partial class NewDB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "NewsUser",
                columns: table => new
                {
                    NewsUserID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NewsUser", x => x.NewsUserID);
                });

            migrationBuilder.CreateTable(
                name: "newsTutor",
                columns: table => new
                {
                    TutorID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    NewsUserID = table.Column<int>(nullable: false),
                    ShotName = table.Column<string>(nullable: true),
                    fullName = table.Column<string>(nullable: true),
                    Gender = table.Column<string>(nullable: true),
                    Tell = table.Column<string>(nullable: true),
                    Facebook = table.Column<string>(nullable: true),
                    courseID = table.Column<int>(nullable: false),
                    Subject = table.Column<string>(nullable: true),
                    Local = table.Column<string>(nullable: true),
                    Hours = table.Column<int>(nullable: false),
                    Money = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_newsTutor", x => x.TutorID);
                    table.ForeignKey(
                        name: "FK_newsTutor_NewsUser_NewsUserID",
                        column: x => x.NewsUserID,
                        principalTable: "NewsUser",
                        principalColumn: "NewsUserID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_newsTutor_NewsUserID",
                table: "newsTutor",
                column: "NewsUserID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "newsTutor");

            migrationBuilder.DropTable(
                name: "NewsUser");
        }
    }
}
