using PSUTutorcheerup.Models;
using Microsoft.EntityFrameworkCore;

namespace PSUTutorcheerup.Data
{
	public class PSUTutorcheerupContext : DbContext
	{
		public DbSet<Tutor> newsTutor { get; set; }

		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
			 optionsBuilder.UseSqlite(@"Data source=Tutor.db");
		}

		public DbSet<PSUTutorcheerup.Models.NewsUser> NewsUser { get; set; }
	}
}