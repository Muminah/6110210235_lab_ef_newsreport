using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using PSUTutorcheerup.Data;
using PSUTutorcheerup.Models;

namespace PSUTutorcheerup.Pages.NewTutor
{
    public class CreateModel : PageModel
    {
        private readonly PSUTutorcheerup.Data.PSUTutorcheerupContext _context;

        public CreateModel(PSUTutorcheerup.Data.PSUTutorcheerupContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
        ViewData["NewsUserID"] = new SelectList(_context.NewsUser, "NewsUserID", "NewsUserID");
            return Page();
        }

        [BindProperty]
        public Tutor Tutor { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.newsTutor.Add(Tutor);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}