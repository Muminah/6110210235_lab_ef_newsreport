using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using PSUTutorcheerup.Data;
using PSUTutorcheerup.Models;

namespace PSUTutorcheerup.Pages.NewTutor
{
    public class DetailsModel : PageModel
    {
        private readonly PSUTutorcheerup.Data.PSUTutorcheerupContext _context;

        public DetailsModel(PSUTutorcheerup.Data.PSUTutorcheerupContext context)
        {
            _context = context;
        }

        public Tutor Tutor { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Tutor = await _context.newsTutor
                .Include(t => t.NewsUse).FirstOrDefaultAsync(m => m.TutorID == id);

            if (Tutor == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
