using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using PSUTutorcheerup.Data;
using PSUTutorcheerup.Models;

namespace PSUTutorcheerup.Pages.NewTutor
{
    public class EditModel : PageModel
    {
        private readonly PSUTutorcheerup.Data.PSUTutorcheerupContext _context;

        public EditModel(PSUTutorcheerup.Data.PSUTutorcheerupContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Tutor Tutor { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Tutor = await _context.newsTutor
                .Include(t => t.NewsUse).FirstOrDefaultAsync(m => m.TutorID == id);

            if (Tutor == null)
            {
                return NotFound();
            }
           ViewData["NewsUserID"] = new SelectList(_context.NewsUser, "NewsUserID", "NewsUserID");
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(Tutor).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TutorExists(Tutor.TutorID))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool TutorExists(int id)
        {
            return _context.newsTutor.Any(e => e.TutorID == id);
        }
    }
}
