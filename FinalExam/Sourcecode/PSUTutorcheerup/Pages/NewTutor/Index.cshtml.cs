using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using PSUTutorcheerup.Data;
using PSUTutorcheerup.Models;

namespace PSUTutorcheerup.Pages.NewTutor
{
    public class IndexModel : PageModel
    {
        private readonly PSUTutorcheerup.Data.PSUTutorcheerupContext _context;

        public IndexModel(PSUTutorcheerup.Data.PSUTutorcheerupContext context)
        {
            _context = context;
        }

        public IList<Tutor> Tutor { get;set; }

        public async Task OnGetAsync()
        {
            Tutor = await _context.newsTutor
                .Include(t => t.NewsUse).ToListAsync();
        }
    }
}
