using System;
using System.ComponentModel.DataAnnotations;

namespace PSUTutorcheerup.Models
{
	public class NewsUser
	{
		public int NewsUserID { get; set; }
		public string FirstName { get; set; }
        public string LastName { get; set; }
	}

	public class Tutor
	{
		public int TutorID { get; set;}

		public int NewsUserID { get; set; }
		public NewsUser NewsUse { get; set; }

		public string ShotName { get; set; }
        public string fullName { get; set; }
		public string Gender { get; set; }

        public string Tell { get; set; }
        public string Facebook { get; set; }

		public int courseID { get; set; }
		public string Subject { get; set; }
		public string Local { get; set; }
		public int Hours { get; set; }
		public int Money { get; set; }

	}
}